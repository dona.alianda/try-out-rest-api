package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"],
		beego.ControllerComments{
			Method: "CreateTest",
			Router: `/create-test`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"],
		beego.ControllerComments{
			Method: "GetListTest",
			Router: `/get-tests`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"],
		beego.ControllerComments{
			Method: "GetTestById",
			Router: `/get-test/:id`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"],
		beego.ControllerComments{
			Method: "UpdateTest",
			Router: `/update-test/:id`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:AdminController"],
		beego.ControllerComments{
			Method: "DeleteTest",
			Router: `/delete-test/:id`,
			AllowHTTPMethods: []string{"delete"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"],
		beego.ControllerComments{
			Method: "StartTest",
			Router: `/start-test`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"],
		beego.ControllerComments{
			Method: "GetQuestions",
			Router: `/get-questions`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"],
		beego.ControllerComments{
			Method: "SubmitAnswer",
			Router: `/submit-answer`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"] = append(beego.GlobalControllerRouter["try-out-rest-api/controllers:StudentController"],
		beego.ControllerComments{
			Method: "CompleteTest",
			Router: `/complete-test`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

}
