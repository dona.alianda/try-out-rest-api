package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"try-out-rest-api/models"
	"try-out-rest-api/models/shared"
)

// Operations about Student
type AdminController struct {
	beego.Controller
}

// @Title CreateTest
// @Description admin create test
// @Param body	body models.CreateTest true "body for create test content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /create-test [post]
func (this *AdminController) CreateTest() {
	var test models.TestRequest

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil || test.TestName == "" {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.CreateTest: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	err = models.CreateTest(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.CreateTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Test created successfully", nil)
	this.ServeJSON()
}

// @Title GetListTest
// @Description admin get test list
// @Param body	body models.GetListTestRequest true "body for test list content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /get-tests [get]
func (this *AdminController) GetListTest() {
	limit := this.GetString("limit")
	offset := this.GetString("offset")

	result, err := models.GetListTest(limit, offset)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.GetListTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Get Tests Success", result)
	this.ServeJSON()
}

// @Title GetTestById
// @Description admin get test by ID
// @Param body	body models.GetListTestRequest true "body for test list content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /get-test/:id [get]
func (this *AdminController) GetTestById() {
	id := this.GetString(":id")

	result, err := models.GetTestById(id)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.GetTestById: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Get Tests Success", result)
	this.ServeJSON()
}

// @Title UpdateTest
// @Description admin update test by ID
// @Param body	body models.UpdateTest true "body for start test content"
// @Success 200 {int} models.Test.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /update-test/:id [put]
func (this *AdminController) UpdateTest() {
	var test models.UpdateTestRequest
	id, _ := this.GetInt(":id")
	test.Id = id

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil || test.TestName == "" {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.UpdateTest: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	err = models.UpdateTest(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.UpdateTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Test updated successfully", nil)
	this.ServeJSON()
}

// @Title DeleteTest
// @Description admin delete test by ID
// @Param body	body models.DeleteTest true "body for start test content"
// @Success 200 {int} models.Test.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /delete-test/:id [delete]
func (this *AdminController) DeleteTest() {
	id, _ := this.GetInt(":id")

	err := models.DeleteTest(id)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.DeleteTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Test deleted successfully", nil)
	this.ServeJSON()
}

// @Title CreateQuestion
// @Description admin create question
// @Param body	body models.CreateQuestion true "body for create question content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /create-test [post]
func (this *AdminController) CreateQuestion() {
	var test models.QuestionRequest

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.CreateQuestion: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	err = models.CreateQuestion(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.CreateQuestion: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Question created successfully", nil)
	this.ServeJSON()
}

// @Title UpdateTest
// @Description admin update question by ID
// @Param body	body models.UpdateTest true "body for start test content"
// @Success 200 {int} models.Test.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /update-question/:id [put]
func (this *AdminController) UpdateQuestion() {
	var test models.QuestionRequest
	id, _ := this.GetInt(":id")
	test.Id = id

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.UpdateQuestion: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	err = models.UpdateQuestion(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.UpdateQuestion: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Question updated successfully", nil)
	this.ServeJSON()
}

// @Title DeleteQuestion
// @Description admin delete test by ID
// @Param body	body models.DeleteTest true "body for start test content"
// @Success 200 {int} models.Test.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /delete-question/:id [delete]
func (this *AdminController) DeleteQuestion() {
	id, _ := this.GetInt(":id")

	err := models.DeleteTest(id)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Admin.DeleteQuestion: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Question deleted successfully", nil)
	this.ServeJSON()
}
