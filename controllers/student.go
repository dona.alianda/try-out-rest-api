package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"try-out-rest-api/models"
	"try-out-rest-api/models/shared"
)

// Operations about Student
type StudentController struct {
	beego.Controller
}

// @Title StartTest
// @Description student post start time to start test
// @Param body	body models.StartTestRequest true "body for start test content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /start-test [post]
func (this *StudentController) StartTest() {
	var test models.StartTestRequest

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.StartTest: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	result, err := models.StartTest(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.StartTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Time Start Now", result)
	this.ServeJSON()
}

// @Title GetQuestions
// @Description student get test question list
// @Param body	body models.GetQuestionResponse true "body for question list content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /get-questions [get]
func (this *StudentController) GetQuestions() {
	limit := this.GetString("limit")
	offset := this.GetString("offset")

	result, err := models.GetQuestions(limit, offset)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.GetQuestions: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Get Question Success", result)
	this.ServeJSON()
}

// @Title SubmitAnswer
// @Description student post answer
// @Param body	body models.SubmitAnswerRequest true "body for  content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /submit-answer [post]
func (this *StudentController) SubmitAnswer() {
	var answer models.SubmitAnswerRequest
	var err error

	err = json.Unmarshal(this.Ctx.Input.RequestBody, &answer)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.SubmitAnswer: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	err = models.SubmitAnswer(answer)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.StartTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Your Answer Has Been Submited", nil)
	this.ServeJSON()
}

// @Title CompleteTest
// @Description student post to complete test
// @Param body	body models.CompleteTest true "body for  content"
// @Success 200 {int} models.Driver.Id
// @Failure 400 bad request
// @Failure 409 conflict
// @router /complete-test [post]
func (this *StudentController) CompleteTest() {
	var test models.CompleteTestRequest
	var err error

	err = json.Unmarshal(this.Ctx.Input.RequestBody, &test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.CompleteTest: "+shared.ERROR_400, err)
		this.ServeJSON()
		return
	}

	result, err := models.CompleteTest(test)
	if err != nil {
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = models.ResponseWithError(400, shared.JSON_ERROR, "Controllers.Student.CompleteTest: "+err.Error(), err)
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.ResponseWithSuccess(200, shared.STATUS_SUCCESS, "Congrrats for Your Result !", result)
	this.ServeJSON()
}
