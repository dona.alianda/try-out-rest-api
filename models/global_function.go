package models

import (
	"github.com/astaxie/beego"
	"try-out-rest-api/models/shared"
)

func ResponseWithError(code int, status string, message string, err error) shared.JSONResponse {
	if err != nil {
		beego.Error(err.Error())
	}
	return shared.JSONResponse{
		Meta: shared.JSONResponseMeta{
			Code:    code,
			Status:  status,
			Message: message,
		},
	}
}

func ResponseWithSuccess(code int, status string, message string, data interface{}) shared.JSONResponse {
	response := shared.JSONResponse{
		Meta: shared.JSONResponseMeta{
			Code:    code,
			Status:  status,
			Message: message,
		},
	}
	if data != nil {
		response.Data = data
	}
	return response
}

func ResponseSuccessWithPagination(code int, status string, message string, data interface{}, results, pages int64) shared.JSONResponse {
	response := shared.JSONResponse{
		Meta: shared.JSONResponseMeta{
			Code:    code,
			Status:  status,
			Message: message,
		},
		TotalResult: int64(results),
		TotalPage:   int64(pages),
	}
	if data != nil {
		response.Data = data
	}
	return response
}
