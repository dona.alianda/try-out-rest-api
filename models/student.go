// student
package models

import (
	"log"
	"strconv"
	"strings"
	"time"
)

type (
	StartTestRequest struct {
		Nis       string    `json:"nis" valid:"Required"`
		StartedAt time.Time `json:"started_at"`
		ExpiredAt time.Time `json:"expired_at"`
		Status    string    `json:"status"`
	}

	GetQuestionResponse struct {
		QuestionId string `json:"question_id`
		Question   string `json:"question"`
		OptionA    string `json:"a."`
		OptionB    string `json:"b."`
		OptionC    string `json:"c."`
	}

	SubmitAnswerRequest struct {
		Nis        string  `json:"nis" valid:"Required"`
		QuestionId int     `json:"question_id" valid:"Required"`
		Answer     string  `json:"answer"`
		Score      float32 `json:"score"`
	}

	QuestionAnswer struct {
		Answer string `json:"answer"`
	}

	ScoreResponse struct {
		Score float32 `json:"score"`
	}

	CompleteTestRequest struct {
		Nis        string    `json:"nis" valid:"Required"`
		FinishedAt time.Time `json:"finished_at"`
		Status     string    `json:"status"`
	}

	CompleteTestResponse struct {
		Nis                    string `json:"nis"`
		TotalQuestion          string `json:"total_question"`
		TotalRightAnswer       string `json:"total_right_answer"`
		TotalWrongAnswer       string `json:"total_wrong_answer"`
		TotalNotAnswer         string `json:"total_not_answered"`
		PercetageCorrectAnswer string `json:"percentage_correct_answer"`
		Duration               string `json:"duration"`
		TotalScore             string `json:"score"`
	}
)

func StartTest(request StartTestRequest) (response StartTestRequest, err error) {
	request.StartedAt = time.Now()
	request.ExpiredAt = request.StartedAt.Add(15 * time.Minute)
	request.Status = "start"

	tx, err := db.Begin()
	if err != nil {
		log.Println("error connection", err.Error())
		return response, err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"INSERT INTO try_out_durations ",
			"(nis, started_at, finished_at, expired_at, status) VALUES(?, ?, ?, ?, ?) "}, " "))

		if err != nil {
			log.Println("error query ", err.Error())
			return response, err
		} else {
			_, err = query.Exec(request.Nis, request.StartedAt, request.ExpiredAt, request.ExpiredAt, request.Status)
			if err != nil {
				log.Println("error parameter ", err.Error())
				return response, err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	response.Nis = request.Nis
	response.StartedAt = request.StartedAt
	response.ExpiredAt = request.ExpiredAt
	response.Status = request.Status

	return response, nil
}

func GetQuestions(limit, offset string) ([]GetQuestionResponse, error) {
	var (
		intLimit  int
		intOffset int
	)

	if limit == "" {
		intLimit = 10
	} else {
		intLimit, _ = strconv.Atoi(limit)
	}

	if offset == "" {
		intOffset = 0
	} else {
		intOffset, _ = strconv.Atoi(offset)
	}

	var questions []GetQuestionResponse = []GetQuestionResponse{}
	var err error

	// query get all questions, return all questions
	rows, err := db.Query("SELECT a.question_id, b.question, a.a, a.b, a.c FROM multiple_choices a LEFT JOIN questions b on a.question_id = b.id LIMIT ? OFFSET ? ", intLimit, intOffset)
	if err != nil {
		// error message
		log.Println("error quesry ", err.Error())
		return questions, err
	} else {
		for rows.Next() {
			var que = GetQuestionResponse{}
			err = rows.Scan(&que.QuestionId, &que.Question, &que.OptionA, &que.OptionB, &que.OptionC)
			questions = append(questions, que)
		}
	}

	return questions, nil
}

func SubmitAnswer(request SubmitAnswerRequest) (err error) {
	answer := GetTrueAnswer(request.QuestionId)
	if request.Answer == answer.Answer {
		request.Score = 4.0
	} else if request.Answer == "" {
		request.Score = 0.0
	} else {
		request.Score = -2.0
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println("error connection", err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"INSERT INTO student_answers ",
			"(nis, question_id, answer, score) VALUES(?, ?, ?, ?) "}, " "))

		if err != nil {
			log.Println("error query ", err.Error())
			return err
		} else {
			_, err = query.Exec(request.Nis, request.QuestionId, request.Answer, request.Score)
			if err != nil {
				log.Println("error parameter ", err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return nil
}

func GetTrueAnswer(questionId int) QuestionAnswer {
	var answer QuestionAnswer

	// query get answer by question id, return request answer
	err := db.QueryRow("SELECT answer FROM questions WHERE id = ?", questionId).Scan(&answer.Answer)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return answer
}

func CountRightAnswer(nis string) ScoreResponse {
	var value ScoreResponse

	// query count total  by right answer, return total right answer
	err := db.QueryRow("SELECT COUNT(*)  FROM `student_answers` WHERE `nis` = ? AND `score` = 4", nis).Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CountWrongAnswer(nis string) ScoreResponse {
	var value ScoreResponse

	// query count total  by wrong answer, return total wrong  answer
	err := db.QueryRow("SELECT COUNT(*)  FROM `student_answers` WHERE `nis` = ? AND `score` = -2", nis).Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CountNotAnswered(nis string) ScoreResponse {
	var value ScoreResponse

	// query count total  by not answered, return total not answered
	err := db.QueryRow("SELECT COUNT(*)  FROM `student_answers` WHERE `nis` = ? AND `score` = 0", nis).Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CountTotalScore(nis string) ScoreResponse {
	var value ScoreResponse

	// query count total  score by nis, return total score by nis
	err := db.QueryRow("SELECT SUM(score) FROM `student_answers` WHERE `nis` = ?", nis).Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CountQuestion() ScoreResponse {
	var value ScoreResponse

	// query count total  by not answered, return total not answered
	err := db.QueryRow("SELECT COUNT(*)  FROM `questions` ").Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CountDuration(nis string) ScoreResponse {
	var value ScoreResponse

	// query count total  duration by nis, return total duration by nis
	err := db.QueryRow("SELECT TIMESTAMPDIFF(SECOND, `started_at`,`finished_at`) FROM `try_out_durations` WHERE `nis`=?", nis).Scan(&value.Score)

	if err != nil {
		// error message
		log.Println(err.Error())
	}

	return value
}

func CompleteTest(request CompleteTestRequest) (response CompleteTestResponse, err error) {
	request.FinishedAt = time.Now()
	request.Status = "finish"
	var (
		currentStatus string
		registeredNIS string
	)

	//check NIS
	err = db.QueryRow("SELECT nis FROM  `students` WHERE `nis`=?", request.Nis).Scan(&registeredNIS)
	if err != nil {
		// error message
		log.Println(err.Error())
	}

	if registeredNIS == "" {
		log.Println("NIS not registered !")
		return response, err
	}

	// check latest status
	err = db.QueryRow("SELECT status FROM  `try_out_durations` WHERE `nis`=?", request.Nis).Scan(&currentStatus)
	if err != nil {
		// error message
		log.Println(err.Error())
	}

	if currentStatus != request.Status {
		// query update duration by nis, return error (if any)
		tx, err := db.Begin()
		if err != nil {
			log.Println(err.Error())
			return response, err
		} else {
			defer tx.Rollback()
			query, err := tx.Prepare(strings.Join([]string{"UPDATE try_out_durations SET ",
				" finished_at = ?",
				" , status = ?",
				" WHERE nis = ?",
			}, " "))

			if err != nil {
				log.Println(err.Error())
				return response, err
			} else {
				_, err = query.Exec(request.FinishedAt, request.Status, request.Nis)
				if err != nil {
					log.Println(err.Error())
					return response, err
				} else {
					err = tx.Commit()
				}
			}
			query.Close()
		}
	}

	totalQuestion := CountQuestion()
	totalRightAnswer := CountRightAnswer(request.Nis)
	totalWrongAnswer := CountWrongAnswer(request.Nis)
	totalNotAnswer := CountNotAnswered(request.Nis)
	totalDuration := CountDuration(request.Nis)
	percentageCorrectAnswer := (totalRightAnswer.Score / totalQuestion.Score) * 100
	totalScore := CountTotalScore(request.Nis)

	var minDuration int
	var secDuration int
	minDuration = int(totalDuration.Score) / 60
	secDuration = int(totalDuration.Score) - (minDuration * 60)

	strTotalQuestion := strconv.FormatFloat(float64(totalQuestion.Score), 'f', 0, 64)
	strTotalRightAnswer := strconv.FormatFloat(float64(totalRightAnswer.Score), 'f', 0, 64)
	strTotalWrongAnswer := strconv.FormatFloat(float64(totalWrongAnswer.Score), 'f', 0, 64)
	strTotalNotAnswer := strconv.FormatFloat(float64(totalNotAnswer.Score), 'f', 0, 64)
	strPercentage := strconv.FormatFloat(float64(percentageCorrectAnswer), 'f', 2, 64)
	strTotalScore := strconv.FormatFloat(float64(totalScore.Score), 'f', 2, 64)

	response.Nis = request.Nis
	response.TotalQuestion = strTotalQuestion
	response.TotalRightAnswer = strTotalRightAnswer
	response.TotalWrongAnswer = strTotalWrongAnswer
	response.TotalNotAnswer = strTotalNotAnswer
	response.Duration = strconv.Itoa(minDuration) + " minute " + strconv.Itoa(secDuration) + " second"
	response.PercetageCorrectAnswer = strPercentage + "% => (Total Question / Total Right Answer * 100)"
	response.TotalScore = strTotalScore

	return response, err

}
