package shared

import ()

//CONSTANT FOR JSON
const (
	JSON_ERROR     = "Invalid Json Input"
	ERROR_400      = "Bad Request"
	STATUS_SUCCESS = "Success"
)

// JSON structure for response
type JSONResponse struct {
	Meta JSONResponseMeta `json:"meta"`

	Data interface{} `json:"data,omitempty"`

	TotalResult int64  `json:"total_result,omitempty"`
	TotalPage   int64  `json:"total_page,omitempty"`
	NextToken   string `json:"next_token,omitempty"`
	PrevToken   string `json:"prev_token,omitempty"`
}

// JSON structure for response Meta
type JSONResponseMeta struct {
	Code    int    `json:"code"`
	Status  string `json:"status"`
	Message string `json:"message"`
}
