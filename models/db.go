package models

import (
	"database/sql"
	_ "fmt"
	"github.com/astaxie/beego"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

var (
	db *sql.DB
)

func InitDB() {
	var (
		err error
	)

	//configuration for DB
	db_user := beego.AppConfig.String("database_user")
	db_pass := beego.AppConfig.String("database_password")
	db_name := beego.AppConfig.String("database_name")
	db_host := beego.AppConfig.String("database_host")
	db_port := beego.AppConfig.String("database_port")

	var connectionString = db_user + ":" + db_pass + "@tcp(" + db_host + ":" + db_port + ")/" + db_name + "?timeout=90s"

	db, err = sql.Open("mysql", connectionString)
	if err != nil {
		log.Println("failed to connect database : ", err)
		panic(err.Error())
	}
}
