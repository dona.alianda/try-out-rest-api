// admin
package models

import (
	"log"
	"strconv"
	"strings"
	"time"
)

type (
	TestRequest struct {
		TestName   string    `json:"test_name" valid:"Required"`
		CretaedAt  time.Time `json:"created_at"`
		Updated_at time.Time `json:"updated_at"`
	}

	TestResponse struct {
		TestName string `json:"test_name"`
	}

	TestDetail struct {
		Id         int    `json:"id"`
		TestName   string `json:"test_name"`
		CretaedAt  string `json:"created_at"`
		Updated_at string `json:"updated_at"`
	}

	UpdateTestRequest struct {
		Id         int       `json:"id" valid:"Required"`
		TestName   string    `json:"test_name" valid:"Required"`
		Updated_at time.Time `json:"updated_at"`
	}

	QuestionRequest struct {
		Id       int    `json:"id" valid:"Required"`
		Question string `json:"question"`
		Answer   string `json:"answer"`
		Note     string `json:"note"`
	}
)

func CreateTest(request TestRequest) (err error) {
	request.CretaedAt = time.Now()
	request.Updated_at = time.Now()

	tx, err := db.Begin()
	if err != nil {
		log.Println("error connection", err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"INSERT INTO tests ",
			"(test_name, created_at, updated_at) VALUES(?, ?, ?) "}, " "))

		if err != nil {
			log.Println("error query ", err.Error())
			return err
		} else {
			_, err = query.Exec(request.TestName, request.CretaedAt, request.Updated_at)
			if err != nil {
				log.Println("error parameter ", err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return nil
}

func GetListTest(limit, offset string) ([]TestResponse, error) {
	var (
		intLimit  int
		intOffset int
	)

	if limit == "" {
		intLimit = 10
	} else {
		intLimit, _ = strconv.Atoi(limit)
	}

	if offset == "" {
		intOffset = 0
	} else {
		intOffset, _ = strconv.Atoi(offset)
	}

	var test []TestResponse = []TestResponse{}
	var err error

	// query get all questions, return all questions
	rows, err := db.Query("SELECT test_name FROM tests LIMIT ? OFFSET ? ", intLimit, intOffset)
	if err != nil {
		// error message
		log.Println("error quesry ", err.Error())
		return test, err
	} else {
		for rows.Next() {
			var que = TestResponse{}
			err = rows.Scan(&que.TestName)
			test = append(test, que)
		}
	}

	return test, nil
}

func GetTestById(id string) (TestDetail, error) {
	var (
		intIdt int
	)

	if id == "" {
		intIdt = 10
	} else {
		intIdt, _ = strconv.Atoi(id)
	}

	var test TestDetail = TestDetail{}
	var err error

	// query get all questions, return all questions
	err = db.QueryRow("SELECT id, test_name, created_at, updated_at FROM tests WHERE id = ? ", intIdt).Scan(&test.Id, &test.TestName, &test.CretaedAt, &test.Updated_at)
	if err != nil {
		// error message
		log.Println("error query ", err.Error())
		return test, err
	}

	return test, nil
}

func UpdateTest(request UpdateTestRequest) (err error) {
	request.Updated_at = time.Now()

	// query update test  by id, return error (if any)
	tx, err := db.Begin()
	if err != nil {
		log.Println(err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"UPDATE tests SET ",
			" test_name = ?",
			" , updated_at = ?",
			" WHERE id = ?",
		}, " "))

		if err != nil {
			log.Println(err.Error())
			return err
		} else {
			_, err = query.Exec(request.TestName, request.Updated_at, request.Id)
			if err != nil {
				log.Println(err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return nil
}

func DeleteTest(id int) error {

	// query delete test by test id, return error (if any)
	tx, err := db.Begin()
	if err != nil {
		log.Println(err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"DELETE FROM tests ",
			" WHERE id = ?",
		}, " "))

		if err != nil {
			log.Println(err.Error())
			return err
		} else {
			_, err = query.Exec(id)
			if err != nil {
				log.Println(err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return err
}

func CreateQuestion(request QuestionRequest) (err error) {

	tx, err := db.Begin()
	if err != nil {
		log.Println("error connection", err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"INSERT INTO questions ",
			"(question, answer, note) VALUES(?, ?, ?) "}, " "))

		if err != nil {
			log.Println("error query ", err.Error())
			return err
		} else {
			_, err = query.Exec(request.Question, request.Answer, request.Note)
			if err != nil {
				log.Println("error parameter ", err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return nil
}

func UpdateQuestion(request QuestionRequest) (err error) {
	// query update question  by id, return error (if any)
	tx, err := db.Begin()
	if err != nil {
		log.Println(err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"UPDATE questions SET ",
			" question = ?",
			" , answer = ?",
			" , note = ?",
			" WHERE id = ?",
		}, " "))

		if err != nil {
			log.Println(err.Error())
			return err
		} else {
			_, err = query.Exec(request.Question, request.Answer, request.Note, request.Id)
			if err != nil {
				log.Println(err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return nil
}

func DeleteQuestion(id int) error {

	// query delete question by test id, return error (if any)
	tx, err := db.Begin()
	if err != nil {
		log.Println(err.Error())
		return err
	} else {
		defer tx.Rollback()
		query, err := tx.Prepare(strings.Join([]string{"DELETE FROM questions ",
			" WHERE id = ?",
		}, " "))

		if err != nil {
			log.Println(err.Error())
			return err
		} else {
			_, err = query.Exec(id)
			if err != nil {
				log.Println(err.Error())
				return err
			} else {
				err = tx.Commit()
			}
		}
		query.Close()
	}

	return err
}
