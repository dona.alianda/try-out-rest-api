# try-out-rest-api API

## Overview

### Application Requirement
* [`Go Lang 1.8.1`](https://golang.org/dl/) as Tools/Programming language

### Installation
#### Set Environment Variables (GOPATH)
The GOPATH environment variable specifies the location of your workspace. It is likely the only environment variable you'll need to set when developing Go code.

To get started, create a workspace directory and set GOPATH accordingly. Your workspace can be located wherever you like, but we'll use $HOME/go in this project.
```
$ export GOPATH=$HOME/go
```

#### Clone **try-out-rest-api** project from repository (this repository)
The project must be cloned under $GOPATH/src directory.

```
$ cd $GOPATH/src
$ git clone [repository URL.git]
```

## Build & Run API
### Application Configuration
In **conf/app.conf**, you must provide **port** to be used and **database** configuration
#### Application Port
```
port = 8080
```

#### Database **try_out_db.sql**
Database try_out_db.sql must be imported.

#### Database Configuration
```
database_user = root
database_password = 
database_name = try_out_db
database_host = localhost
database_port = 3306
```

#### Build API
```
cd $GOPATH/src/try-out-rest-api
go build
```

#### Run API
```
chmod +x try-out-rest-api
nohup ./try-out-rest-api &
```
