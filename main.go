package main

import (
	"github.com/astaxie/beego"
	"try-out-rest-api/models"
	_ "try-out-rest-api/routers"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	models.InitDB()

	beego.Run()
}
