-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21 Mei 2018 pada 13.00
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `try_out_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `multiple_choices`
--

CREATE TABLE `multiple_choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `a` varchar(100) NOT NULL,
  `b` varchar(100) NOT NULL,
  `c` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `multiple_choices`
--

INSERT INTO `multiple_choices` (`id`, `question_id`, `a`, `b`, `c`) VALUES
(1, 1, 'Kemanusiaan Yang Adil dan Beradab', 'Persatuan Indonesia', 'Ketuhanan Yang Maha Esa'),
(2, 2, 'KH.Agus Salim', 'Tri Sutrisno', 'Adam Malik'),
(3, 3, 'A.H Nasution', 'Jendral Soedirman', 'A.Yani'),
(4, 4, 'B.J Habibie', 'Ki Hajar Dewantara', 'KH.Agus Salim'),
(5, 5, 'Hiroshima & Tokyo', 'Hiroshima & Nagasaki', 'Tokyo & Nagasaki'),
(6, 6, 'Mengalir dari tempat tinngi ke tempat lebih rendah', 'Menekan ke segala arah', 'Isolator terhadap listrik'),
(7, 7, 'Ovopipivar', 'Ovivar', 'Piparovo'),
(8, 8, 'Tanah dan Pupuk', 'Klorofil & H2O', 'Karbondioksida & Cahaya Matahari'),
(9, 9, 'Metamorfosis', 'Evolusi', 'Reproduksi'),
(10, 10, 'H2SO4', 'Asam Sulfat', 'Keduanya betul'),
(11, 11, 'Monolog', 'Prolog', 'Epilog'),
(12, 12, 'Buah jatuh tidak jauh dari pohonnya', 'Anak ayam yg kehilangan induknya', 'Guru kencing berdiri murid kencing berlari'),
(13, 13, 'Homograf', 'Homonim', 'Homofon'),
(14, 14, 'Personifikasi', 'Hiperbola', 'Perumpamaan'),
(15, 15, 'Pergi !', 'Aku Pergi', 'Pergi.'),
(16, 16, '2', '1', '0'),
(17, 17, '4', '1', '0'),
(18, 18, '6', '1', '0'),
(19, 19, '2', '1', '8'),
(20, 20, '10', '1', '0'),
(21, 21, '2', '12', '0'),
(22, 22, '2', '1', '14'),
(23, 23, '2', '16', '0'),
(24, 24, '2', '18', '0'),
(25, 25, '2', '1', '20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(300) NOT NULL,
  `answer` char(1) NOT NULL,
  `note` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`, `note`) VALUES
(1, '1 dari 5 sila pernah diubah dalam Piagam Jakarta tahun 1948. Sila ke berapakah yang pernah diubah di piagam Jakarta ?', 'c', 'Sila pertama, Ketuhanan Yang Maha Esa'),
(2, 'ASEAN merupakan perkumpulan negara-negara Asia Tenggara yg pada awalnya didirikan oleh 5 negara dengan masing-masing perwakilan negara. Sipakah yang mewakili Indonesia di acara pembentukan ASEAN pertama kalinya ?', 'c', 'Adam Malik'),
(3, 'G30S PKI begitu banyak memakan korban termasuk 7 Jenderal yg pada saat 1965 itu menjabat. Ada seoran', 'a', 'Abdul Haris Nasution'),
(4, 'Pendidikan di Indonesia dikenal memiliki 3 semboyan yang sangat bermakna, Tut Wuri Handayani, Ing Ma', 'b', 'Ki Hajar Dewantara'),
(5, 'Jepang sempat dibuat matisuri oleh Amerika dengan peristiwa pengeboman bom atom yang fenomenal di 2 ', 'b', 'HIrishima & Nagasaki'),
(6, 'Hal berikut yg tidak termasuk sifat-sifat air ?', 'c', 'Termasuk isolator terhdap listrik'),
(7, 'Beberapa hewan dikenal bisa berkembang biak dengan 2 cara baik bertelur atau melahirkan, seperti pada beberapa spesies ular. Hal ini dikenal dengan istilah ?', 'a', 'Ovovivipar'),
(8, 'Tumbuhan mampu membuat makanannya sendiri atau lebih dikenal dengan fotosintesis. Berikut unsur yg tidak dibtuhkan ketika fotosintesis', 'a', 'Tanah & pupuk'),
(9, 'Beberapa mahluk hidup melewati phase-phase pertumbuhan & perkembangan yang berbeda dari satu phase ke phase yang lainnya. Hal ini disebut ?', 'a', 'Metamorfosis'),
(10, 'Beberapa zat akan berubah menjadi asam ketika bereaksi dengan air. Jika Sulfur (S) bereaksi dengan air akan membentuk?', 'c', 'Keduanya betul'),
(11, 'Drama merupakan suatu karya seni dimana karakter, tempat dan bahasa diatur sedemikian rupa untuk menceritkan suatu kisah baik fiksi maupun nonfiksi. Kata-kata pembuka yang disampaikan dalam drama biasa disebut ?', 'b', 'Prolog'),
(12, 'Peribahasa yang tepat untuk karakter seorang anak yang tidak jauh beda dengan karakter orang tuanya adalah ?', 'a', 'Buah jatuh tidak jauh dari pohonnya'),
(13, 'Kata yang memiliki penulisan sama persis tapi beda dengan pelafalannya disebut dengan istikah ?', 'c', 'Homofon'),
(14, 'Majas atau gaya bahasa yang menggambarkan segala sesuatunya itu berlebihan disebut ?', 'b', 'Hiperbola'),
(15, 'Berikut yang tidak termasuk kalimat sempurna menurut Bahasa Indonesia yang baik dan benar adalah ?', 'c', 'pergi.'),
(16, '1+1=?', 'a', '2'),
(17, '2+2=?', 'a', '4'),
(18, '3+3=?', 'a', '6'),
(19, '4+4=?', 'c', '8'),
(20, '5+5=?', 'a', '10'),
(21, '6+6=?', 'b', '12'),
(22, '7+7=?', 'c', '14'),
(23, '8+8=?', 'b', '16'),
(24, '9+9=?', 'b', '18'),
(25, '10+10=?', 'c', '20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL COMMENT 'id is primary for students',
  `nis` varchar(10) NOT NULL COMMENT 'nis is the second primary for student number',
  `name` varchar(100) NOT NULL COMMENT 'student fullname'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `students`
--

INSERT INTO `students` (`id`, `nis`, `name`) VALUES
(1, '07081125', 'Dona Alianda'),
(2, '07081126', 'Humaira Noorlaila'),
(3, '07081127', 'Doni Aliandi'),
(4, '07081128', 'Nur Rahma ''Aisyah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `student_answers`
--

CREATE TABLE `student_answers` (
  `id` int(11) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` char(1) NOT NULL,
  `score` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `student_answers`
--

INSERT INTO `student_answers` (`id`, `nis`, `question_id`, `answer`, `score`) VALUES
(1, '07081125', 1, 'c', 4),
(2, '07081125', 2, 'c', 4),
(3, '07081125', 3, 'a', 4),
(4, '07081125', 4, 'b', 4),
(5, '07081125', 5, 'b', 4),
(6, '07081125', 6, 'c', 4),
(7, '07081125', 7, 'a', 4),
(8, '07081125', 8, 'a', 4),
(9, '07081125', 9, 'a', 4),
(10, '07081125', 10, 'c', 4),
(11, '07081125', 11, 'b', 4),
(12, '07081125', 12, 'a', 4),
(13, '07081125', 13, 'c', 4),
(14, '07081125', 14, 'b', 4),
(15, '07081125', 15, 'c', 4),
(16, '07081125', 16, 'a', 4),
(17, '07081125', 17, 'a', 4),
(18, '07081125', 18, 'a', 4),
(19, '07081125', 19, 'c', 4),
(20, '07081125', 20, 'a', 4),
(21, '07081125', 21, 'b', 4),
(22, '07081125', 22, 'c', 4),
(23, '07081125', 23, 'b', 4),
(24, '07081125', 24, 'b', -2),
(25, '07081125', 25, '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `test_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tests`
--

INSERT INTO `tests` (`id`, `test_name`, `created_at`, `updated_at`) VALUES
(3, 'Multiple Choice', '2018-05-21 03:17:02', '2018-05-21 03:17:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `try_out_durations`
--

CREATE TABLE `try_out_durations` (
  `id` int(11) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `started_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finished_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `try_out_durations`
--

INSERT INTO `try_out_durations` (`id`, `nis`, `started_at`, `finished_at`, `expired_at`, `status`) VALUES
(2, '07081126', '2018-05-19 23:50:07', '2018-05-20 00:05:07', '2018-05-20 00:05:07', 'start'),
(3, '07081127', '2018-05-19 23:50:24', '2018-05-20 00:05:24', '2018-05-20 00:05:24', 'start'),
(4, '07081128', '2018-05-19 23:50:50', '2018-05-20 18:38:50', '2018-05-20 00:05:31', 'finish'),
(6, '07081125', '2018-05-20 18:03:29', '2018-05-20 18:19:31', '2018-05-20 18:18:29', 'finish');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `multiple_choices`
--
ALTER TABLE `multiple_choices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`,`nis`);

--
-- Indexes for table `student_answers`
--
ALTER TABLE `student_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `try_out_durations`
--
ALTER TABLE `try_out_durations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `multiple_choices`
--
ALTER TABLE `multiple_choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id is primary for students', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `student_answers`
--
ALTER TABLE `student_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `try_out_durations`
--
ALTER TABLE `try_out_durations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
